import unittest
from app.pad654 import find_time_to_break


class TestPad654(unittest.TestCase):

    def test_find_time_to_break(self):
        self.assertEqual(find_time_to_break(0, 30), 50.23)
        self.assertEqual(find_time_to_break(0, 90), 18.38)
        self.assertEqual(find_time_to_break(0, 120), 15.01)
        self.assertEqual(find_time_to_break(0, 180), 12.8)
