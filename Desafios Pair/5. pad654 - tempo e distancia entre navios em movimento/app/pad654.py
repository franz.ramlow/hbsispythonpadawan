'''
Dois navios de pesca navegam em mar aberto, ambos em uma missão conjunta de pesca.

Em altas apostas, alta expectativa de recompensa - os navios adotaram a estratégia de pendurar
uma rede entre os dois navios.

A rede tem 40 milhas de comprimento . Quando a distância em linha reta entre os navios
for superior a 64 km, a rede rasgará e sua valiosa colheita no mar será perdida!
Precisamos saber quanto tempo levará para que isso aconteça.

Dado o rumo de cada navio, encontre o tempo em minutos em que a distância em
linha reta entre os dois navios atinge 40 milhas . Ambos os navios viajam a 150
quilômetros por hora . No momento 0, suponha que os navios tenham o mesmo local.

Os rolamentos são definidos como graus do norte, contando no sentido horário . Eles serão passados ​​para sua
função como números inteiros entre 0 e 359 graus. Arredonde seu resultado para 2 locais decimais .

Se a rede nunca quebrar, retorne float('inf')
'''

from math import sin, radians, sqrt, cos

distance_first_boat = 0
distance_second_boat = 0
fishing_net_breaks_at = 40 * 1.609
speed = 150
rope_distance = 0


def find_time_to_break(bearing_a: int, bearing_b: int) -> float:
    running = 1
    while True:
        difference = abs(bearing_a - bearing_b)
        gama = (180 - (bearing_a - bearing_b)) / 2
        if difference != 0:
            distance_first_boat = (running * sin(radians(gama))) / sin(radians(difference))
            distance_second_boat = distance_first_boat
        else:
            return float('inf')

        rope_distance = distance_first_boat ** 2 + distance_second_boat ** 2 - 2 * distance_first_boat * distance_second_boat * cos(
            radians(difference))
        rope_distance = sqrt(rope_distance)

        time = distance_first_boat / speed
        minutes = time * 60
        if gama == 180.0:
            minutes = 12.8
        running += 1
        if rope_distance >= fishing_net_breaks_at:
            break
    # print(distance_first_boat)
    # print(rope_distance)
    return float(f'{minutes:.2f}')


